﻿using Microsoft.Practices.Unity;

namespace ThoughtWorksProblemStatement_1
{
    /// <summary>
    /// Single instance object
    /// Use 'Current' property to get current Container
    /// </summary>
    public static class Container
    {
        public readonly static UnityContainer Current;
        static Container()
        {
            Current = new UnityContainer();
        }
    }
}
