﻿using System.Collections.Generic;
using ThoughtWorksProblemStatement_1.Exceptions;
using ThoughtWorksProblemStatement_1.Models;

namespace ThoughtWorksProblemStatement_1.Business
{
    public static class TollTrack
    {
        private static Dictionary<string, int> _vehicleEntries = new Dictionary<string, int>();
        public static void Entry(string vehicleNumber, int entryPoint)
        {
            try
            {
                _vehicleEntries.Add(vehicleNumber, entryPoint);
            }
            catch
            {
                throw new InvalidTollEntryException();
            }

        }

        public static TravelDetails Exit(string vehicleNumber, int exitPoint)
        {
            int entryPoint;
            if (!_vehicleEntries.TryGetValue(vehicleNumber, out entryPoint))
                throw new InvalidTollExitException();

            _vehicleEntries.Remove(vehicleNumber);

            return new TravelDetails { EntryPoint = entryPoint, ExitPoint = exitPoint, VehicleNumber = vehicleNumber };
        }
    }
}
