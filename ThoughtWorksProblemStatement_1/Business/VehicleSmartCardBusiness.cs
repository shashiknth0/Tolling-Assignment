﻿using ThoughtWorksProblemStatement_1.DataAccess;

namespace ThoughtWorksProblemStatement_1.Business
{
    public class VehicleSmartCardBusiness
    {
        private readonly IVehicleSmartCardRepository _vehicleRepo;
        public VehicleSmartCardBusiness(IVehicleSmartCardRepository vehicleRepo)
        {
            _vehicleRepo = vehicleRepo;
        }
        public void RegisterCard(string vehicleNumber, string vehicleType, string owner, float amount)
        {
            _vehicleRepo.RegisterSmartCard(vehicleNumber, vehicleType, owner, amount);
        }
    }
}
