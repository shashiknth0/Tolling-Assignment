﻿using System.Collections.Generic;

namespace ThoughtWorksProblemStatement_1.Business.Validations
{
    public class Validator<T>
    {
        private readonly List<IValidationRule<T>> _rules = new List<IValidationRule<T>>();

        public Validator<T> AddRule(IValidationRule<T> rule)
        {
            _rules.Add(rule);
            return this;
        }

        public bool Validate(T data)
        {
            bool isValid = true;

            foreach (var rule in _rules)
            {
                isValid &= rule.Validate(data);

                if (!isValid) break;
            }

            return isValid;
        }
    }
}
