﻿namespace ThoughtWorksProblemStatement_1.Business.Validations
{
    public interface IValidationRule<in T>
    {
        bool Validate(T data);
    }
}
