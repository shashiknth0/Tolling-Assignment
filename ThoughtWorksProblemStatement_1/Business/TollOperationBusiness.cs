﻿using System.Linq;
using Microsoft.Practices.Unity;
using ThoughtWorksProblemStatement_1.Business.Validations;
using ThoughtWorksProblemStatement_1.DataAccess;
using ThoughtWorksProblemStatement_1.Models;

namespace ThoughtWorksProblemStatement_1.Business
{
    public class TollOperationBusiness
    {
        private readonly IVehicleSmartCardRepository _vehicleSmartCardRepository;

        private Validator<SmartCard> _smartCardValidator;
        private Validator<SmartCard> SmartCardValidator
        {
            get
            {
                if (_smartCardValidator == null)
                {
                    _smartCardValidator = new Validator<SmartCard>()
                                            .AddRule(new CardAuthenticityRule())
                                            .AddRule(new CardValidityRule())
                                            .AddRule(new CardMinBalanceRule());
                }

                return _smartCardValidator;
            }
        }

        public TollOperationBusiness(IVehicleSmartCardRepository vehicleSmartCardRepository)
        {
            _vehicleSmartCardRepository = vehicleSmartCardRepository;
        }
        public bool TollEntry(string vehicleNumber, int tollgateNumber)
        {
            bool isValid = true;
            var smartCards = _vehicleSmartCardRepository.Get(vehicleNumber);

            //Validate the Smartcard
            foreach (var smartCard in smartCards)
            {
                isValid &= SmartCardValidator.Validate(smartCard);

                if (isValid) break;
            }

            //None of the card is valid
            if (!isValid) return false;

            //Entry for Track
            TollTrack.Entry(vehicleNumber, tollgateNumber);

            return true;
        }

        public FareDetails TollExit(string vehicleNumber, int tollgateNumber)
        {
            TravelDetails travelDetails = TollTrack.Exit(vehicleNumber, tollgateNumber);
            SmartCard smartCard = _vehicleSmartCardRepository.Get(vehicleNumber).First();

            var fareHandler = Container.Current.Resolve<FareHandler>();

            float fareAmount = fareHandler.GetFare(smartCard.VehicleType, travelDetails.EntryPoint, travelDetails.ExitPoint);
            smartCard = _vehicleSmartCardRepository.DeductAmount(smartCard.Id, fareAmount);

            return new FareDetails { Balance = smartCard.Amount, Commuter = smartCard.Commuter, RideFare = fareAmount };
        }
    }
}
