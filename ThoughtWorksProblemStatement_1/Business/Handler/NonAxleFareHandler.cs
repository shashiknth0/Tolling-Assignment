﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using ThoughtWorksProblemStatement_1.Road;

namespace ThoughtWorksProblemStatement_1.Business
{
    public class NonAxleFareHandler : FareHandler
    {
        private readonly string[] _accepts;
        public NonAxleFareHandler(params string[] acceptableTypes)
        {
            _accepts = acceptableTypes;
        }

        public override float GetFare(string vehicleType, int from, int to)
        {
            if (_accepts.Contains(vehicleType))
            {
                var routeGraph = Container.Current.Resolve<RouteGraph<int>>(vehicleType);
                return routeGraph.FindPathWeight(from, to);
            }
            else if (this.Successor != null)
            {
                return this.Successor.GetFare(vehicleType, from, to);
            }

            throw new NotSupportedException("No handler for Vehicle type");
        }
    }
}
