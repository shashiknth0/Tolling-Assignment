﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using ThoughtWorksProblemStatement_1.Road;

namespace ThoughtWorksProblemStatement_1.Business
{
    public class AxleFareHandler : FareHandler
    {
        private readonly string[] _accepts;

        private readonly float _baseCost;
        private readonly float _baseRate;
        public AxleFareHandler(float baseCost, float rate, params string[] acceptableTypes)
        {
            _baseCost = baseCost;
            _baseRate = rate;
            _accepts = acceptableTypes;
        }

        public override float GetFare(string vehicleType, int from, int to)
        {
            if (_accepts.Contains(vehicleType))
            {
                var routeGraph = Container.Current.Resolve<RouteGraph<int>>(vehicleType);
                var distanceTravelled = routeGraph.FindPathWeight(from, to);
                return _baseCost + distanceTravelled * _baseRate;
            }
            else if (this.Successor != null)
            {
                return this.Successor.GetFare(vehicleType, from, to);
            }

            throw new NotSupportedException("No handler for Vehicle type");
        }
    }
}
