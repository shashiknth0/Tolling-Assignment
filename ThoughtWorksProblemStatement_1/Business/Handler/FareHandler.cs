﻿namespace ThoughtWorksProblemStatement_1.Business
{
    public abstract class FareHandler
    {
        protected FareHandler Successor;
        public FareHandler AddSuccessor(FareHandler handler)
        {
            Successor = handler;

            return handler;
        }

        public abstract float GetFare(string vehicleType, int from, int to);
        
    }
}
