﻿using System.Collections.Generic;
using System.Linq;

namespace ThoughtWorksProblemStatement_1.Road
{
    public class RouteGraph<T>
    {
        public List<T> Tolls { get; set; }
        protected List<float> Costs { get; set; }

        public RouteGraph(T[] tolls, float[] costs = null)
        {
            Tolls = tolls.ToList();

            if (costs == null)
                costs = new float[tolls.Length];

            Costs = costs.ToList();
        }

        public float FindPathWeight(T from, T to)
        {
            float totalCost = 0f;
            int betweenTolls = 0;

            int fromNode = Tolls.IndexOf(from);
            int toNode = Tolls.IndexOf(to);
            betweenTolls = toNode - fromNode;

            if (fromNode > toNode)
                betweenTolls = Tolls.Count + betweenTolls;

            for (int i = 0; i < betweenTolls; i++)
            {
                totalCost += Costs[(fromNode + i) % Tolls.Count];
            }

            return totalCost;
        }
    }

    public class WeightedRouteGraph<T> : RouteGraph<T>
    {
        public WeightedRouteGraph(RouteGraph<T> graph, float[] weights) : base(graph.Tolls.ToArray(), weights)
        {

        }

    }

}
