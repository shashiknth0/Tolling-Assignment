﻿using Microsoft.Practices.Unity;
using ThoughtWorksProblemStatement_1.Business;
using ThoughtWorksProblemStatement_1.DataAccess;
using ThoughtWorksProblemStatement_1.Road;

namespace ThoughtWorksProblemStatement_1
{
    static class AppSetup
    {
        internal static void Initialize()
        {
            RouteInitialize();
            FareHandlerInitialize();
            DependencyRegister();
        }

        private static void DependencyRegister()
        {
            Container.Current.RegisterType<IVehicleSmartCardRepository, VehicleSmartCardRepository>();
        }

        private static void RouteInitialize()
        {
            RouteGraph<int> routeMap = new RouteGraph<int>(tolls: new int[] { 1, 2, 3, 4 });
            var distance = new WeightedRouteGraph<int>(routeMap, new float[] { 55, 45, 40, 41 });//Distance in Kms
            var lmvCost = new WeightedRouteGraph<int>(routeMap, new float[] { 60, 50, 45, 40 });
            var lcvCost = new WeightedRouteGraph<int>(routeMap, new float[] { 100, 90, 80, 70 });

            Container.Current.RegisterInstance<RouteGraph<int>>("LMV", lmvCost);
            Container.Current.RegisterInstance<RouteGraph<int>>("LCV", lcvCost);
            Container.Current.RegisterInstance<RouteGraph<int>>("2A", distance);
            Container.Current.RegisterInstance<RouteGraph<int>>("3A", distance);
            Container.Current.RegisterInstance<RouteGraph<int>>("4A", distance);
            Container.Current.RegisterInstance<RouteGraph<int>>("5A", distance);
            Container.Current.RegisterInstance<RouteGraph<int>>("6A", distance);
            Container.Current.RegisterInstance<RouteGraph<int>>("7A", distance);
        }



        private static void FareHandlerInitialize()
        {
            FareHandler fareHandler = new AxleFareHandler(100, 5, new string[] { "2A", "3A", "4A", "5A" });
            fareHandler.AddSuccessor(new AxleFareHandler(150, 7, new string[] { "6A", "7A" }))
                       .AddSuccessor(new NonAxleFareHandler(new string[] { "LMV", "LCV" }));

            Container.Current.RegisterInstance<FareHandler>(fareHandler);
        }

    }
}
