﻿using System.Collections.Generic;
using ThoughtWorksProblemStatement_1.Models;

namespace ThoughtWorksProblemStatement_1.DataAccess
{
    public interface IVehicleSmartCardRepository
    {
        bool RegisterSmartCard(string vehicleNumber, string vehicleType, string owner, float amount);

        SmartCard DeductAmount(int cardId, float amount);

        List<SmartCard> Get(string vehicleNumber);
    }
}