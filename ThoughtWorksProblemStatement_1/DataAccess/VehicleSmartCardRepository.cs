﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ThoughtWorksProblemStatement_1.Exceptions;
using ThoughtWorksProblemStatement_1.Models;

namespace ThoughtWorksProblemStatement_1.DataAccess
{
    public class VehicleSmartCardRepository : IVehicleSmartCardRepository
    {
        private static List<SmartCard> dataStore;
        private static int currentId;
        private int AutoId { get { return Interlocked.Increment(ref currentId); } }

        static VehicleSmartCardRepository()
        {
            dataStore = new List<SmartCard>();
            currentId = 0;
        }
        public List<SmartCard> Get(string vehicleNumber)
        {
            //TODO:
            return dataStore.Where(x=>x.VehicleNumber.Equals(vehicleNumber,StringComparison.CurrentCultureIgnoreCase)).ToList();
        }

        public bool RegisterSmartCard(string vehicleNumber, string vehicleType, string owner, float amount)
        {
            dataStore.Add(new SmartCard
            {
                Id = AutoId,
                VehicleNumber = vehicleNumber,
                VehicleType = vehicleType,
                Commuter = owner,
                Amount = amount
            });

            return true;
        }

        public SmartCard DeductAmount(int cardId, float amount)
        {
            var card = dataStore.FirstOrDefault(x => x.Id == cardId);

            if (card.Amount < amount)
                throw new InsufficientFundsException($"Insufficient funds i.e < {amount}");

            card.Amount = card.Amount - amount;
            dataStore.RemoveAll(x => x.Id == cardId);
            dataStore.Add(card);

            return card;
        }
    }
}
