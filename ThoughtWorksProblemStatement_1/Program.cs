﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using ThoughtWorksProblemStatement_1.Business;
using System.Text.RegularExpressions;

namespace ThoughtWorksProblemStatement_1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //AppSetup
                //1. Setup Route, Rates etc
                AppSetup.Initialize();

                //2. Vehicle Smart Card Registration
                SmartCardRegistration();

                //3. Process the Tolls
                TollProcess();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unexpected error occured.. please reach out to the provider \n press Any key to terminate");
                Console.ReadKey();
            }

        }

        private static void TollProcess()
        {
            Console.WriteLine("Enter Toll Entries/Exits \n ex: {Vehicle Number} {Tollgate Number}.. \n Enter 'Quit' upon completion \n");

            while (true)
            {
                string entered = Console.ReadLine();
                if (entered.Equals("quit", StringComparison.OrdinalIgnoreCase))
                    break;

                var tollDetails = entered.Split(' ');

                RunToll(tollDetails[0], tollDetails[1]);
            }
        }

        private static void RunToll(string vehicleNumber, string tollAction)
        {
            Regex expression = new Regex(@"([a-zA-Z]+)(\d+)");
            Match result = expression.Match(tollAction);

            string action = result.Groups[1].Value;
            int gateNumber = int.Parse(result.Groups[2].Value);

            var tollBusiness = Container.Current.Resolve<TollOperationBusiness>();

            if (action.Equals("EX", StringComparison.CurrentCultureIgnoreCase))
            {
                var fareDetails = tollBusiness.TollExit(vehicleNumber, gateNumber);
                Console.WriteLine($"Commuter Name: {fareDetails.Commuter}, balance deducted: {fareDetails.RideFare}, remaining balance: {fareDetails.Balance}");
            }
            else
            {
                tollBusiness.TollEntry(vehicleNumber, gateNumber);
            }


        }

        private static void SmartCardRegistration()
        {
            Console.WriteLine("Enter Vehicle Smart Card Info \n ex: {Vehicle Number} {Vehicle type} {Commuter Name} {Adv Amount}.. \n Enter 'Quit' upon completion \n");

            var smardCardBusiness = Container.Current.Resolve<VehicleSmartCardBusiness>();

            while (true)
            {
                string entered = Console.ReadLine();
                if (entered.Equals("quit", StringComparison.OrdinalIgnoreCase))
                    break;

                try
                {
                    var smartCardInfo = entered.Split(' ');
                    smardCardBusiness.RegisterCard(smartCardInfo[0], smartCardInfo[1], smartCardInfo[2], float.Parse(smartCardInfo[3]));
                }
                catch
                {
                    Console.WriteLine("Unable register.. Please try with valid input");
                }
            }
        }
    }
}
