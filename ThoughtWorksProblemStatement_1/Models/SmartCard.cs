﻿namespace ThoughtWorksProblemStatement_1.Models
{
    public class SmartCard
    {
        public int Id { get; set; }
        public string VehicleNumber { get; set; }
        public string VehicleType { get; set; }
        public string Commuter { get; set; }
        public float Amount { get; set; }
    }
}