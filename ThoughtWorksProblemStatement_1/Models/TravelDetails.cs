﻿namespace ThoughtWorksProblemStatement_1.Models
{
    public class TravelDetails
    {
        public string VehicleNumber { get; set; }
        public int ExitPoint { get; set; }
        public int EntryPoint { get; set; }
    }
}
