﻿namespace ThoughtWorksProblemStatement_1.Models
{
    public class FareDetails
    {
        public string Commuter { get; set; }
        public float RideFare { get; set; }
        public float Balance { get; set; }
    }
}
